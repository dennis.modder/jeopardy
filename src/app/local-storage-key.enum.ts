export enum LocalStorageKey {
  GAMES = 'jeopardy_games',
  LANG = 'jeopardy_lang',
}
