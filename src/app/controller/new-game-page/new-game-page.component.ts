import {Component, OnInit} from '@angular/core';
import {LocalStorageService} from '../../local-storage.service';
import {Game} from '../../game';
import {QuestionsInputFile} from '../../questions-input-file';
import {QuestionCategory} from '../../question-category';
import {Router} from '@angular/router';
import {LocalStorageKey} from '../../local-storage-key.enum';
import {Team} from '../../team';
import * as uuid from 'uuid';

@Component({
  selector: 'app-new-game-page',
  templateUrl: './new-game-page.component.html',
  styleUrls: ['./new-game-page.component.scss']
})
export class NewGamePageComponent {

  public game: Game;

  constructor(private localStorageService: LocalStorageService, private router: Router) {
  }

  public saveGame() {
    if (this.game) {
      let games: string | null = this.localStorageService.getValue(LocalStorageKey.GAMES);
      this.localStorageService.setValue(this.game.id, JSON.stringify(this.game));
      if (games === null) {
        games = '[]';
        this.localStorageService.setValue(LocalStorageKey.GAMES, games);
      }
      const str: string[] = JSON.parse(games);
      str.push(this.game.id);
      this.localStorageService.setValue(LocalStorageKey.GAMES, JSON.stringify(str));
      this.router.navigate(['/controller/edit/' + this.game.id]);
    }
  }

  /**
   * Read question file
   * @param e event
   */
  public readQuestionFile(e) {
    const file = e.target.files[0];
    if (!file) {
      return;
    }
    try {
      const reader = new FileReader();
      reader.onload = (event) => {
        this.parseQuestionFile(event.target.result);
      };
      reader.readAsText(file);
    } catch (ex) {
      console.error(ex.message);
    }
  }

  /**
   * Parse question file
   * @param content file content
   */
  private parseQuestionFile(content: string | ArrayBuffer) {
    const game: Game = {id: '', name: '', questions: [], teams: [], activeTeam: undefined};
    const obj: QuestionsInputFile = JSON.parse(content.toString());
    const team: Team = {id: uuid.v4(), name: 'Gruppe 1', color: '#ff0000', points: 0};
    game.id = uuid.v4();
    game.questions = this.parseQuestionObject(obj);
    game.name = obj.name;
    game.teams.push(team);
    game.activeTeam = team;
    this.game = game;
    return game;
  }

  private parseQuestionObject(obj: QuestionsInputFile): QuestionCategory[] {
    const category: QuestionCategory[] = [];
    obj.questionCategories.forEach((cat: QuestionCategory) => {
      category.push({
        name: cat.name,
        questions: cat.questions.map(q => Object.assign({won: 0, id: uuid.v4()}, q))
      } as unknown as QuestionCategory);
    });
    return category;
  }
}
