import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ControllerPageComponent} from './controller-page/controller-page.component';
import {NewGamePageComponent} from './new-game-page/new-game-page.component';
import {EditGamePageComponent} from './edit-game-page/edit-game-page.component';
import {GameControlPageComponent} from './game-control-page/game-control-page.component';

const routes: Routes = [
  {path: 'controller',  component: ControllerPageComponent},
  {path: 'controller/new',  component: NewGamePageComponent},
  {path: 'controller/edit/:id',  component: EditGamePageComponent},
  {path: 'controller/game/:id',  component: GameControlPageComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule
  ]
})
export class ControllerRoutingModule {
}
