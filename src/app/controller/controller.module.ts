import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ControllerPageComponent} from './controller-page/controller-page.component';
import {ControllerRoutingModule} from './controller-routing.module';
import { NewGamePageComponent } from './new-game-page/new-game-page.component';
import { EditGamePageComponent } from './edit-game-page/edit-game-page.component';
import { GameControlPageComponent } from './game-control-page/game-control-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    ControllerPageComponent,
    NewGamePageComponent,
    EditGamePageComponent,
    GameControlPageComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ControllerRoutingModule,
        TranslateModule
    ]
})
export class ControllerModule { }
