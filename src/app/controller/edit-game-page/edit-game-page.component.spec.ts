import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditGamePageComponent } from './edit-game-page.component';

describe('EditGamePageComponent', () => {
  let component: EditGamePageComponent;
  let fixture: ComponentFixture<EditGamePageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGamePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGamePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
