import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalStorageService} from '../../local-storage.service';
import {Game} from '../../game';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {LocalStorageKey} from '../../local-storage-key.enum';
import * as uuid from 'uuid';

@Component({
  selector: 'app-edit-game-page',
  templateUrl: './edit-game-page.component.html',
  styleUrls: ['./edit-game-page.component.scss']
})
export class EditGamePageComponent implements OnInit {

  public game: Game;

  public gameForm = this.fb.group({
    gameName: ['', Validators.required],
    teams: this.fb.array([]),
  });

  constructor(private route: ActivatedRoute, private router: Router, private lss: LocalStorageService, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.game = JSON.parse(this.lss.getValue(id));
    this.initForm();
  }

  get teams() {
    return this.gameForm.get('teams') as FormArray;
  }

  public addTeam() {
    this.teams.push(this.fb.group({
      id: [uuid.v4()],
      name: [''],
      color: [''],
      points: ['0']
    }));
  }

  public deleteTeam(index) {
    const control = this.gameForm.controls.teams as FormArray;
    control.removeAt(index);
    // todo: Gruppe ist aktive Gruppe -> auf nächste aktive Gruppe verschieben
  }

  public async deleteGame() {
    const games: string | null = this.lss.getValue(LocalStorageKey.GAMES);
    const str: string[] = JSON.parse(games);
    const index = str.indexOf(this.game.id);
    if (index > -1) {
      str.splice(index, 1);
      this.lss.setValue(LocalStorageKey.GAMES, JSON.stringify(str));
      this.lss.remove(this.game.id);
      await this.router.navigate(['controller']);
    }
  }

  private initForm() {
    if (this.game !== null){
      this.game.teams.forEach(() => {
        this.addTeam();
      });
      this.gameForm.patchValue({
        gameName: this.game.name,
        teams: this.game.teams,
      });
    }
  }

  public async onFormSubmit() {
    console.log("x");
    this.game.name = this.gameForm.value.gameName;
    this.game.teams = this.gameForm.value.teams;
    this.lss.setValue(this.game.id, JSON.stringify(this.game));
    await this.router.navigate(['controller']);
  }

}
