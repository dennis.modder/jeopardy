import {Component} from '@angular/core';
import {LocalStorageService} from '../../local-storage.service';
import {Game} from '../../game';
import {LocalStorageKey} from '../../local-storage-key.enum';

@Component({
  selector: 'app-controller-page',
  templateUrl: './controller-page.component.html',
  styleUrls: ['./controller-page.component.scss']
})
export class ControllerPageComponent {

  public games: Game[] = [];
  public activeGame: Game;

  constructor(private localStorageService: LocalStorageService) {
    let games: string | null = this.localStorageService.getValue(LocalStorageKey.GAMES);
    if (games === null) {
      games = '[]';
      this.localStorageService.setValue(LocalStorageKey.GAMES, games);
    }
    const str: string[] = JSON.parse(games);
    str.forEach((value) => {
      const g: string | null = this.localStorageService.getValue(value);
      if (g !== null) {
        this.games.push(JSON.parse(g));
      }
    });
  }

  public startGame(id: string) {
    const g: string | null = this.localStorageService.getValue(id);
    if (g !== null) {
      this.activeGame = JSON.parse(g);
    }
  }

}
