import {Component, OnInit} from '@angular/core';
import {Game} from '../../game';
import {ActivatedRoute, Router} from '@angular/router';
import {LocalStorageService} from '../../local-storage.service';
import {Question} from '../../question';
import {ChannelService} from '../../channel.service';
import {Team} from '../../team';

@Component({
  selector: 'app-game-control-page',
  templateUrl: './game-control-page.component.html',
  styleUrls: ['./game-control-page.component.scss']
})
export class GameControlPageComponent implements OnInit {

  public game: Game;
  public selectedQuestion: Question;
  public showRatingInput = false;
  public ratingTeamID: string;
  public timer;
  public timerSeconds = 0;

  constructor(private route: ActivatedRoute, private router: Router, private localStorageService: LocalStorageService, private channelService: ChannelService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.game = JSON.parse(this.localStorageService.getValue(id));
    this.game = this.updatePoints(this.game);
    console.log(this.game);
  }

  public openQuestion(id: string) {
    this.game.questions.forEach((cat) => {
      cat.questions.forEach((q) => {
        if (q.id === id) {
          this.selectedQuestion = q;
          return;
        }
      });
    });
  }

  public closeQuestion() {
    this.timerSeconds = 0;
    this.selectedQuestion = undefined;
    this.ratingTeamID = undefined;
    this.showRatingInput = false;
    this.resetTimer();
  }

  public showQuestion() {
    if (this.selectedQuestion) {
      this.timerSeconds = 0;
      this.channelService.postQuestion(this.selectedQuestion);
      this.startTimer();
    }
  }

  public showGameOverView() {
    this.channelService.postGame(this.game);
  }

  public showQuestionAnswer() {
    this.showRatingInput = true;
    this.channelService.postShowAnswer(true);
    this.ratingTeamID = '-1';
    this.resetTimer();
  }

  public changeRatingTeam(event) {
    this.ratingTeamID = event.target.value;
  }

  public savePointsBtnClick() {
    this.timerSeconds = 0;
    this.savePoints();
    this.closeQuestion();
  }

  private savePoints() {
    this.game.questions.forEach((cat) => {
      cat.questions.forEach((q) => {
        if (q.id === this.selectedQuestion.id) {
          if (typeof this.ratingTeamID !== 'undefined') {
            q.won = this.ratingTeamID;
            this.game = this.updatePoints(this.game);
            this.game = this.updateActiveTeam(this.game);
            this.localStorageService.setValue(this.game.id, JSON.stringify(this.game));
            return;
          }
        }
      });
    });
  }

  private updateActiveTeam(game: Game): Game {
    const teams: number = game.teams.length - 1;
    if (teams > 0) {
      let index = 0;
      for (let i = 0; i <= teams; i++) {
        if (game.activeTeam.id === game.teams[i].id) {
          if ((i + 1) <= teams) {
            index = i + 1;
          }
          game.activeTeam = game.teams[index];
          return game;
        }
      }
    }
    return game;
  }

  private updatePoints(game: Game): Game {
    game.teams.forEach((team) => {
      team.points = this.countPointsOfTeam(team);
    });
    return game;
  }

  private countPointsOfTeam(team: Team): number {
    let sum = 0;
    this.game.questions.forEach((cat) => {
      cat.questions.forEach((q) => {
        if (q.won === team.id) {
          sum = sum + q.points;
        }
      });
    });
    return sum;
  }

  private startTimer() {
    this.timerSeconds = 0;
    this.resetTimer();
    this.timer = setInterval(() => {
      this.timerSeconds = this.timerSeconds + 1;
    }, 1000);
  }

  private resetTimer() {
    if (typeof this.timer !== 'undefined') {
      clearInterval(this.timer);
    }
  }

  public translations(){
    return {
      seconds: this.timerSeconds
    };

  }

}
