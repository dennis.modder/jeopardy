import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GameControlPageComponent } from './game-control-page.component';

describe('GameControlPageComponent', () => {
  let component: GameControlPageComponent;
  let fixture: ComponentFixture<GameControlPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GameControlPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameControlPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
