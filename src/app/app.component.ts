import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LocalStorageService} from './local-storage.service';
import {LocalStorageKey} from './local-storage-key.enum';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'jeopardy';

  constructor(private lss: LocalStorageService, private translate: TranslateService, private titleService: Title) {
    const defaultLang = 'en';
    translate.setDefaultLang(defaultLang);
    const currentLang: string = this.lss.getValue(LocalStorageKey.LANG);
    if (!currentLang){
      this.lss.setValue(LocalStorageKey.LANG, defaultLang);
    }
    translate.use(currentLang);
    translate.get('APP_NAME').subscribe((title: string) => {
      this.titleService.setTitle(title);
    });
  }
}
