import { Injectable } from '@angular/core';
import {Channels} from './channels.enum';
import {Game} from './game';
import {Question} from './question';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  private questionChannel = new BroadcastChannel(Channels.QUESTION);
  private gameChannel = new BroadcastChannel(Channels.GAME);
  private showAnswerChannel = new BroadcastChannel(Channels.SHOWANSWER);

  public game: BehaviorSubject<Game> = new BehaviorSubject<Game>(undefined);
  public question: BehaviorSubject<Question> = new BehaviorSubject<Question>(undefined);
  public showAnswer: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {
    this.gameChannel.onmessage = (event) => {
      this.game.next(event.data);
    };
    this.questionChannel.onmessage = (event) => {
      this.question.next(event.data);
    };
    this.showAnswerChannel.onmessage = (event) => {
      this.showAnswer.next(event.data);
    };
  }

  public postQuestion(question: Question) {
    this.questionChannel.postMessage(question);
  }

  public postGame(game: Game) {
    this.gameChannel.postMessage(game);
  }

  public postShowAnswer(showAnswer: boolean) {
    this.showAnswerChannel.postMessage(showAnswer);
  }

}
