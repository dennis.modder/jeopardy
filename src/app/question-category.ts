import {Question} from './question';

export interface QuestionCategory {
  name: string;
  questions: Question[];
}
