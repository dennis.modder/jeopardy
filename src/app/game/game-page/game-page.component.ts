import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ChannelService} from '../../channel.service';
import {Game} from '../../game';
import {Question} from '../../question';

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.scss']
})
export class GamePageComponent implements OnInit {

  public game: Game;
  public question: Question;
  public showAnswer = false;

  constructor(private channelService: ChannelService, private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.channelService.game.subscribe((data) => {
      this.game = data;
      this.question = undefined;
      this.showAnswer = false;
      this.cdRef.detectChanges();
    });
    this.channelService.question.subscribe((data) => {
      this.question = data;
      this.game = undefined;
      this.showAnswer = false;
      this.cdRef.detectChanges();
    });
    this.channelService.showAnswer.subscribe((data) => {
      this.showAnswer = data;
      this.cdRef.detectChanges();
    });
  }

}
