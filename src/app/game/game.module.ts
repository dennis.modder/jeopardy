import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GameRoutingModule} from './game-routing.module';
import {GamePageComponent} from './game-page/game-page.component';
import {QuestionComponent} from './question/question.component';
import { OverviewComponent } from './overview/overview.component';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    GamePageComponent,
    QuestionComponent,
    OverviewComponent,
  ],
    imports: [
        CommonModule,
        GameRoutingModule,
        TranslateModule
    ]
})
export class GameModule { }
