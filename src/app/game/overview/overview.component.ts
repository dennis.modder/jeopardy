import {AfterViewInit, Component, ElementRef, Input, QueryList, ViewChildren} from '@angular/core';
import {Game} from '../../game';
import {Team} from '../../team';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements AfterViewInit {

  public g: Game;
  public questionHeight = 0;

  @Input()
  set game(game: Game) {
    this.g = game;
  }

  @ViewChildren('category') categoryElements: QueryList<ElementRef>;
  @ViewChildren('categoryName') categoryNameElements: QueryList<ElementRef>;
  @ViewChildren('question') QuestionElements: QueryList<ElementRef>;

  constructor() { }

  public isActiveTeam(team: Team): boolean {
    if (typeof team !== 'undefined') {
      console.log(team);
      return (team.id === this.g.activeTeam.id);
    }
    return false;
  }

  ngAfterViewInit() {
    this.setQuestionHeight();
  }


  private setQuestionHeight() {
    let maxHeight = 30;
    let maxQuestions = 0;
    let windowHeight = window.innerHeight;

    this.g.questions.forEach((cat) => {
      if (cat.questions.length > maxQuestions) {
        maxQuestions = cat.questions.length;
      }
    });

    windowHeight = windowHeight - 50; // teams
    windowHeight = windowHeight - 97; // category name
    windowHeight = windowHeight - (maxQuestions * 50); // padding each question

    if (maxQuestions > 0) {
      maxHeight = windowHeight / maxQuestions;
    }

    // set question height
    for (let l = 0; l < this.QuestionElements.toArray().length; l++) {
      this.QuestionElements.toArray()[l].nativeElement.style.height = maxHeight + 'px';
    }
  }
}
