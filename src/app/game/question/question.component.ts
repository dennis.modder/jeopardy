import {Component, Input} from '@angular/core';
import {Question} from '../../question';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent {

  public q: Question;
  public a = false;

  @Input()
  set question(question: Question) {
    this.q = question;
  }

  @Input()
  set showAnswer(showAnswer: boolean) {
    this.a = showAnswer;
  }

}
