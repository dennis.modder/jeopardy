export enum Channels {
  QUESTION = 'jeopardy_question',
  SHOWANSWER = 'jeopardy_showAnswer',
  GAME = 'jeopardy_game'
}
