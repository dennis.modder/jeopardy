import {Team} from './team';
import {QuestionCategory} from './question-category';

export interface Game {
  id: string;
  name: string;
  teams: Team[];
  activeTeam: Team;
  questions: QuestionCategory[];
}
