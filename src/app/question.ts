export interface Question {
  id: string;
  question: string;
  answer: string;
  won: string|number;
  points: number;
}
