import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {LocalStorageKey} from './local-storage-key.enum';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  private storageSub = new Subject<LocalStorageKey | string>();

  constructor() { }

  watchStorage(): Observable<any> {
    return this.storageSub.asObservable();
  }

  public getValue(key: LocalStorageKey | string): string|null {
    const value = localStorage.getItem(key);
    if (typeof value === 'string') {
      return value;
    }
    return null;
  }

  public setValue(key: LocalStorageKey | string, value: string|number|boolean) {
    this.storageSub.next(key);
    localStorage.setItem(key, value.toString());
  }

  public remove(key: LocalStorageKey | string) {
    this.storageSub.next(key);
    localStorage.removeItem(key);
  }
}
