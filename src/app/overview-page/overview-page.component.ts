import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {LocalStorageKey} from '../local-storage-key.enum';
import {LocalStorageService} from '../local-storage.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.scss']
})
export class OverviewPageComponent {

  constructor(private lss: LocalStorageService, private translate: TranslateService, private titleService: Title) { }

  setLanguage(lang: string) {
    this.translate.use(lang);
    this.lss.setValue(LocalStorageKey.LANG, lang);
    this.translate.get('APP_NAME').subscribe((title: string) => {
      this.titleService.setTitle(title);
    });
  }

}
